const express = require('express');
const bodyParser = require('body-parser');
var requests = require('request');

var app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var port = process.env.PORT | 3000;

app.post("/", (req, response) => {
    console.log(req.body);
    if (req.body) {
        let location = 'Chennai';
        let url = `http://api.openweathermap.org/data/2.5/weather?APPID=8a81d247d650cb16469c4ba3ceb7d265&q=${location}`;

        return new Promise((resolve, reject) => {
            requests(url, function(error, responseVal, body) {
                if (error) {
                    reject();
                }
                body = JSON.parse(body);

                console.log(typeof body);
                
                var responseText = "";
                if (body.cod == 200) {
                    responseText = `Currently in ${location}, the weather is: ${body.weather[0].description}`;
                    console.log(`Response text: ${responseText}`);
                    response.json(responseText);
                } else {
                    responseText = `Query was not succesfull.`;
                }
                resolve();
            });
        });

    }
});

app.listen(port, (data) => {
    console.log(`App is now started at ${port}`);
});
